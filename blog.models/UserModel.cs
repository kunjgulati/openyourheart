﻿using System;
using System.ComponentModel.DataAnnotations;
namespace blog.models
{
   public class UserModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string UserCode { get; set; }
        public DateTime? WhenCreated { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Salt { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public AuthorDetailsModel AuthorDetails { get; set; }
    }
}
