﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace blog.models
{
   public class AuthorDetailsModel
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Profession { get; set; }
        public System.DateTime? DOB { get; set; }
        public string Hobbies { get; set; }
    }
}
