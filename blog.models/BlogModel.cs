﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Net;


namespace blog.models
{
   public class BlogModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name ="Blog Title")]
        public string BlogTitle { get; set; }
        public string BlogName { get; set; }
        [Display(Name ="Open Your Heart")]
     
        public string BlogPostData { get; set; }
        public DateTime? WhenCreated { get; set; }
        public DateTime? WhenModified { get; set; }
        public int AuthorId { get; set; }    
        
        public bool IsPublic { get; set; }
        public bool IsActive { get; set; }
        public string PostDataLimitedWords => string.Join(" ", WebUtility.HtmlDecode(BlogPostData).Split(' ').Take(10));
        public AuthorDetailsModel AuthorDetails { get; set; }

        //public string BlogPostDataHTML => WebUtility.HtmlDecode(BlogPostData);
    }
}
