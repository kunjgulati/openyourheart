﻿using System;
using System.Security.Cryptography;

namespace blog.db.Helper
{
    public class SaltHashing
    {
        private static string CreateSalt(int size)
        {
            try
            {
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                byte[] buff = new byte[size];
                rng.GetBytes(buff);
                return Convert.ToBase64String(buff);
            }
            catch (Exception ex)
            {
                throw new Exception("Error while creating salt for password: " + ex.Message);
            }
        }
        private static byte[] GetBytes(string str)
        {
            try
            {
                byte[] bytes = new byte[str.Length * 2 * sizeof(char)];
                str = string.Concat(str, str); //so that even the bytes are unpredictable
                System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
                return bytes;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating string bytes " + ex.Message);
            }
        }

        public static string[] GenerateSaltedHash(string plainText, string salt = null)
        {
            try
            {
                string saltBytes;
                if (!string.IsNullOrEmpty(salt)) // to check existing password
                {
                    saltBytes = salt;
                }
                else
                    saltBytes = CreateSalt(16); //to create new password and salt

                byte[] pass = GetBytes(plainText);
                HashAlgorithm algorithm = new SHA256Managed();
                byte[] plainTextWithSaltBytes =
                  new byte[plainText.Length + saltBytes.Length];
                for (int i = 0; i < plainText.Length; i++)
                {
                    plainTextWithSaltBytes[i] = pass[i];
                }
                for (int i = 0; i < saltBytes.Length; i++)
                {
                    plainTextWithSaltBytes[plainText.Length + i] = Convert.ToByte(saltBytes[i]);
                }
                string[] result = new string[2];
                result[0] = saltBytes;
                result[1] = Convert.ToBase64String(algorithm.ComputeHash(plainTextWithSaltBytes));
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Salted Hash " + ex.Message);
            }
        }

        public static bool VerifyPassword(string plainText, string hashedInput, string salt)
        {
            try
            {
                string[] newHashedPin = GenerateSaltedHash(plainText, salt);
                return newHashedPin[1].Equals(hashedInput);
            }
            catch (Exception ex)
            {
                throw new Exception("Error creating Salted Hash " + ex.Message);
            }
        }
    }
}
