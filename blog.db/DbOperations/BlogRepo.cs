﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using blog.models;

namespace blog.db.DbOperations
{
    public class BlogRepo
    {
        
        public int createBlog(BlogModel blog)
        {
            using (var context = new BlogsProjectEntities())
            {
                BlogDetails blogObj = new BlogDetails()
                {
                    BlogTitle = blog.BlogTitle,
                    BlogName = blog.BlogName,
                    BlogPostData = WebUtility.HtmlEncode(blog.BlogPostData),
                    IsPublic = blog.IsPublic,
                    AuthorId = Convert.ToInt32(HttpContext.Current.Session["userSessionId"]),
                    IsActive = true,
                    WhenCreated = System.DateTime.Now
                };
                context.BlogDetails.Add(blogObj);
                return context.SaveChanges();
            }
        }

        public List<BlogModel> getPublicBlogs()
        {
            using (var context = new BlogsProjectEntities())
            {
                var result = context.BlogDetails
                    .Where(x => x.IsPublic == true && (x.IsActive == true || x.IsActive == null))
                    .Select(x => new BlogModel()
                    {
                        Id = x.Id,
                        BlogTitle = x.BlogTitle,
                        BlogPostData = x.BlogPostData,
                        WhenCreated = x.WhenCreated,
                        AuthorDetails = new AuthorDetailsModel()
                        {
                            FirstName = x.AuthorDetails.FirstName,
                            LastName = x.AuthorDetails.LastName,
                            Profession = x.AuthorDetails.Profession
                        }
                    }).OrderByDescending(x => x.WhenCreated).ToList();

                return result;
                //List<BlogDetails> bloglist = context.BlogDetails.Where(x=>x.IsPublic==true).ToList();
                //return bloglist;
                //have to add user check 
            }
        }

        public BlogModel GetBlogById(int Id)
        {
            using (var context = new BlogsProjectEntities())
            {
                var result = context.BlogDetails
                    .Where(x => x.Id == Id && (x.IsActive == true || x.IsActive==null))
                    .Select(x => new BlogModel()
                    {
                        Id = x.Id,
                        BlogTitle = x.BlogTitle,
                        BlogPostData = x.BlogPostData,
                        WhenCreated = x.WhenCreated,
                        IsPublic = x.IsPublic != null && x.IsPublic == true ? true : false,
                        AuthorId= x.AuthorId,
                        AuthorDetails = new AuthorDetailsModel()
                        {
                            FirstName = x.AuthorDetails.FirstName,
                            LastName = x.AuthorDetails.LastName,
                            Profession = x.AuthorDetails.Profession
                        }
                    }).FirstOrDefault();

                return result;

            }
        }

        public List<BlogModel> getUserBlogs(int userid)
        {
            using (var context = new BlogsProjectEntities())
            {
                var result = context.BlogDetails
                    .Where(x => x.AuthorId == userid && (x.IsActive == true || x.IsActive == null))
                    .Select(x => new BlogModel()
                    {
                        Id = x.Id,
                        BlogTitle = x.BlogTitle,
                        BlogPostData = x.BlogPostData,
                        WhenCreated = x.WhenCreated,
                        IsPublic = x.IsPublic != null && x.IsPublic == true ? true : false,
                        AuthorDetails = new AuthorDetailsModel()
                        {
                            FirstName = x.AuthorDetails.FirstName,
                            LastName = x.AuthorDetails.LastName,
                            Profession = x.AuthorDetails.Profession
                        }
                    }).OrderByDescending(x => x.WhenCreated).ToList();

                return result;
            }
        }

        public int updateBlog(BlogModel model)
        {
            using (var context = new BlogsProjectEntities())
            {
                BlogDetails blogDetails = new BlogDetails()
                {
                    Id = model.Id,
                    BlogTitle = model.BlogTitle,
                    BlogPostData = WebUtility.HtmlEncode(model.BlogPostData),
                    AuthorId = Convert.ToInt32(HttpContext.Current.Session["userSessionId"]),
                    IsPublic = model.IsPublic,
                    WhenCreated = model.WhenCreated!=null ? model.WhenCreated: System.DateTime.Now,
                    WhenModified = System.DateTime.Now,
                    IsActive = true
                };
                context.Entry(blogDetails).State = System.Data.Entity.EntityState.Modified;
              return context.SaveChanges();
            }
        }

        public bool deleteBlog(int Id)
        {
            try
            {
                using (var context = new BlogsProjectEntities())
                {
                    var blogToBeDeleted = context.BlogDetails.FirstOrDefault(x => x.Id == Id); 

                    if(blogToBeDeleted!=null)
                    {
                        blogToBeDeleted.IsActive = false;
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch(Exception ex)
            {
                return false;
                throw new Exception("Something went wrong while Deleting the record:",ex);
            }
        }
        public bool togglePublishBlog(int Id)
        {
            
            try
            {
                using (var context = new BlogsProjectEntities())
                {
                    var blogToBeDeleted = context.BlogDetails.FirstOrDefault(x => x.Id == Id);

                    if (blogToBeDeleted != null)
                    {
                        blogToBeDeleted.IsPublic = blogToBeDeleted.IsPublic == false ? true:false;
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception("Something went wrong while Deleting the record:", ex);
            }
        }
    }
}

