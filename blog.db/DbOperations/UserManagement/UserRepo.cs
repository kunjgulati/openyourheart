﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using blog.models;
namespace blog.db.DbOperations.UserManagement
{
   public class UserRepo
    {
       public int RegisterUser(UserModel user)
        {
            using (var context = new BlogsProjectEntities())
            {
                Users userObj = new Users()
                {
                    UserName = user.AuthorDetails.FirstName + user.AuthorDetails.LastName,
                    Email = user.Email,
                    PasswordHash = user.Password,
                    Salt = user.Salt,
                    WhenCreated = System.DateTime.Now,               
                    
                };
                context.Users.Add(userObj);
                context.SaveChanges();
                int userID = userObj.Id;

                
                if(user!= null && userID>0)
                {
                    AuthorDetails AuthorDetails = new AuthorDetails()
                    {
                        AuthorId = userID,
                        FirstName = user.AuthorDetails.FirstName,
                        LastName = user.AuthorDetails.LastName
                    };

                    context.AuthorDetails.Add(AuthorDetails);
                    context.SaveChanges();
                }

                return userID;
            }


            
        }

        public UserModel GetUserByEmail(string email)
        {
            using (var context = new BlogsProjectEntities())
            {

                var result = context.Users
                     .Where(x => x.Email == email)
                     .Select(x => new UserModel()
                     {
                         Id = x.Id,
                         Email = x.Email,
                         Password = x.PasswordHash,
                         Salt = x.Salt,
                         AuthorDetails = new AuthorDetailsModel()
                         {
                             FirstName = x.AuthorDetails.FirstName,
                             LastName = x.AuthorDetails.LastName,
                             Profession = x.AuthorDetails.Profession
                         }
                     }).FirstOrDefault();

                return result;
            }

        }
    }
}
