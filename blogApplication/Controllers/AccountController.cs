﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using blog.db.DbOperations.UserManagement;
using blog.models;
using blog.db.Helper;
using System.Web.Security;

namespace blogApplication.Controllers
{
    public class AccountController : Controller
    {
        UserRepo userRepo = new UserRepo();
        
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UserModel loginUser)
        {
            if(loginUser.Email !=null && loginUser.Password !=null)
            {
                UserModel userModel = userRepo.GetUserByEmail(loginUser.Email);
                bool isUserVerfied = false;
                if (userModel != null)
                {
                    if (userModel.Password !=null && userModel.Salt !=null)
                        isUserVerfied = SaltHashing.VerifyPassword(loginUser.Password, userModel.Password, userModel.Salt);
                }
                else
                {
                    // ViewBag.errorMessage = "User Authentication failed";
                    ModelState.AddModelError("", "User Doesn't Exists");
                    return View();
                }

                if(isUserVerfied)
                {
                  //  ViewBag.successMessage = "User Logged in";
                    FormsAuthentication.SetAuthCookie(userModel.AuthorDetails.FirstName + " " + userModel.AuthorDetails.LastName, false);
                    Session["userSessionId"] = userModel.Id;
                    return RedirectToAction("GetUserBlogs", "Blog");
                }
                else
                {
                    //ViewBag.errorMessage = "User Authentication failed";
                    ModelState.AddModelError("", "invalid Username or Password");
                }
            }
            return View();
        }
        public ActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(UserModel user)
        {
            if(ModelState.IsValid)
            {
                var hash = SaltHashing.GenerateSaltedHash(user.Password);
                user.Salt = hash[0];
                user.Password = hash[1];

               int userID = userRepo.RegisterUser(user);
                if(userID>0)
                {
                   return RedirectToAction("Login");
                }

            }
            return View();
        }
        
        public ActionResult LogoutUser()
        {
            FormsAuthentication.SignOut();
            Session["userSessionId"] = null;
            return RedirectToAction("Login");
        }
    }
}