﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using blog.models;
using blog.db.DbOperations;
using static blogApplication.Filters.FilterConfig;

namespace blogApplication.Controllers
{
    public class BlogController : Controller
    {
        BlogRepo blogRepo = new BlogRepo();
        // GET: Blog
        [Authorize]
        public ActionResult Create()
        {
           
            return View();
            //var result = blogRepo.GetBlogById(3024);
            //return View(result);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(BlogModel blogDetails)
        {
            if (ModelState.IsValid)
            {
                int id = blogRepo.createBlog(blogDetails);

                if (id > 0)
                {
                    ModelState.Clear();
                    ViewBag.SuccessMessage = "Blog Created!!";
                }
                return RedirectToAction("GetUserBlogs");
            }
            return View();
        }


        public ActionResult GetAllPublicBlogs()
        {
            if (ModelState.IsValid)
            {
                var result = blogRepo.getPublicBlogs();
           
            return View(result);
            }

            return View();
        }

        [Authorize]
        [Route("myblogs")]

        public ActionResult GetUserBlogs()
        {
            //get user id from session
            //int userId = 1;
            int userId = Convert.ToInt32(Session["userSessionId"]);
            var result = blogRepo.getUserBlogs(userId);

            return View(result);
        }

        public ActionResult GetBlogDetails(int Id)
        {           
            var result = blogRepo.GetBlogById(Id);
            if(result!=null)
                return View(result);
            return View();
        }

        [NoDirectAccess]
        [Authorize]
        [Route("edit")]
        public ActionResult EditBlog(int id)
        {
            var result = blogRepo.GetBlogById(id);
            return View(result);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditBlog(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                if (form["editor1"] != null)
                {
                    var model = new BlogModel()
                    {
                        Id = Convert.ToInt32(form["Id"]),
                        BlogTitle = form["BlogTitle"],
                        IsPublic = form["IsPublic"].Contains("true") ? true : false,
                        BlogPostData = form["editor1"],
                        WhenCreated = Convert.ToDateTime(form["WhenCreated"]) != null ? Convert.ToDateTime(form["WhenCreated"]) : System.DateTime.Now
                    };

                    var result = blogRepo.updateBlog(model);
                    if (result > 0)
                        return RedirectToAction("GetBlogDetails", new { Id = model.Id });
                }
            }
            return View();
        }

        [Authorize]
        public ActionResult DeleteBlog(int id)
        {
            if (ModelState.IsValid)
            {
                var deleted = blogRepo.deleteBlog(id);    
                if(deleted)
                   return RedirectToAction("GetUserBlogs");
                return View();
            }
            return View();
        }
        [Authorize]
        public ActionResult TogglePublishBlog(int id)
        {
            if (ModelState.IsValid)
            {
                var publishToggleSuccess = blogRepo.togglePublishBlog(id);
                if (publishToggleSuccess)
                    return RedirectToAction("GetUserBlogs");
                return View();
            }
            return View();
        }
    }
}